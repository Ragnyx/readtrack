﻿using Api.Controllers.Base;
using Api.Data.Wrapper;
using Api.Models;
using Api.Models.DTO;
using Api.Models.Entities;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Controllers
{
    public class IssuesController : BaseApiController
    {
        private IRepositoryWrapper _repoWrapper;
        private readonly IMapper _mapper;

        public IssuesController(IRepositoryWrapper repoWrapper,IMapper mapper)
        {
            _repoWrapper = repoWrapper;
            _mapper = mapper;

        }

        [HttpGet("{id}")]
        public async Task<ActionResult<IssueDto>> GetIssue(int id)
        {
            return await _repoWrapper.Issues.GetIssueDto(id);
        }

        [HttpPost]
        public async Task<ActionResult<IssueDto>> SaveIssue(IssueDto issueDto)
        {
            Issue issue = _mapper.Map<Issue>(issueDto);
            issue.Created = DateTime.Now;

            if(issueDto.Observers.Count() > 0)
            {
                foreach(MemberDto observer in issueDto.Observers)
                {
                    var user = await _repoWrapper.Users.GetUserByUsernameAsync(observer.Username);

                    //AppUser appuser = _mapper.Map<AppUser>(observer);
                    
                    var issueObserver = new IssueObserver
                    {
                        Issue = issue,
                        Observer = user,
                        ObserverId = user.Id
                    };

                    issue.ObservingUsers.Add(issueObserver);
                    user.ObservingIssues.Add(issueObserver);
                }
            }

            Project project = _repoWrapper.Projects.Find(issue.IssueInProjectId);
            project.ProjectIssues.Add(issue);
            issue.IssueInProject = project;

            if (await _repoWrapper.Save()) return NoContent();
            return BadRequest("Failed to create issue");
        }
    }
}
