﻿using Api.Controllers.Base;
using Api.Data;
using Api.Data.Wrapper;
using Api.Extensions;
using Api.Models;
using Api.Models.DTO;
using Api.Models.Entities;
using Api.Models.Helpers;
using Api.Services.Files;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Api.Controllers
{

    public class UsersController : BaseApiController
    {
        private IRepositoryWrapper _repoWrapper;
        private readonly IMapper _mapper;
        private readonly IPhotoService _photoService;

        public UsersController(IRepositoryWrapper repoWrapper,IMapper mapper,IPhotoService photoService)
        {
            _repoWrapper = repoWrapper;
            _mapper = mapper;
            _photoService = photoService;
        }

        [HttpGet]
        [Authorize(Roles = "Programmer,Employee")]
        public ActionResult<IEnumerable<MemberDto>> GetUsers()
        {
            var users = _repoWrapper.Users.GetUsers().ToList();
            var usersToReturn = _mapper.Map<IEnumerable<MemberDto>>(users);

            return Ok(usersToReturn);
        }

        [Authorize(Roles = "Manager")]
        [HttpGet("{username}", Name = "GetUser")]
        public  ActionResult<MemberDto> GetUser(string username)
        {
            return _mapper.Map<MemberDto>(_repoWrapper.Users.GetUser(username));
        }

        [HttpPut]
        public async Task<ActionResult> UpdateUser(MemberUpdateDto memberUpdateDto)
        {
            var user = _repoWrapper.Users.GetUser(User.GetUsername());
            int v = User.GetUserId();

            _mapper.Map(memberUpdateDto, user);
            _repoWrapper.Users.Update(user);
            if (await _repoWrapper.Save()) return NoContent();

            return BadRequest("Failed to update user");
        }

        [HttpPost("add-photo")]
        [Authorize]
        public async Task<ActionResult<PhotoDto>> AddPhoto(IFormFile file)
        {
            var user = _repoWrapper.Users.GetUser(User.GetUsername());
            var result = await _photoService.AddPhotoAsync(file);

            if (result.Error != null)
            {
                return BadRequest(result.Error.Message);
            }

            var photo = new Photo
            {
                Url = result.SecureUrl.AbsoluteUri,
                PublicId = result.PublicId
            };

            if(user.Photo != null)
            {
              await DeletePhoto();
            }

            user.Photo = photo;

            if (await _repoWrapper.Save())
                return CreatedAtRoute("GetUser", new { username = user.UserName }, _mapper.Map<PhotoDto>(photo));

            return BadRequest("Problem adding photo");
        }

        [HttpDelete("delete-photo")]
        [Authorize]
        public async Task<ActionResult> DeletePhoto()
        {
            var user = await _repoWrapper.Users.GetUserByUsernameAsync(User.GetUsername());
            var photo = user.Photo;
            if(photo == null)
            {
                return NotFound();
            }

            if(photo.PublicId != null)
            {
                var result = await _photoService.DeletePhotoAsync(photo.PublicId);
                if (result.Error != null) return BadRequest(result.Error.Message);
            }

            user.Photo = null;
            if(await _repoWrapper.Save())
            {
                return Ok();
            }

            return BadRequest("Failed to delete photo");
        }


        [HttpGet("user-projects")]
        public  ActionResult<IEnumerable<ProjectDto>> GetUserProjects([FromQuery] ProjectParams projectParams)
        {
            var sourceUserId = User.GetUserId();
            IQueryable<Project> projects = _repoWrapper.ProjectUserRepository.GetUserProjects(sourceUserId);


            PagedList<ProjectDto> projectDtos = PagedList<ProjectDto>.CreateAsync(
                projects.ProjectTo<ProjectDto>(_mapper.ConfigurationProvider)
                , projectParams.PageNumber, projectParams.PageSize);

            Response.AddPaginationHeader(projectDtos.CurrentPage, projectDtos.PageSize, projectDtos.TotalCount, projectDtos.TotalPages);

            return Ok(projects);
        }
    }
}
