﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Api.Controllers.Base;
using Api.Data;
using Api.Data.Wrapper;
using Api.Models;
using Api.Models.DTO;
using Api.Services.Authorization;
using Api.Services.Token;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Api.Controllers
{
    public class AccountController : BaseApiController
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly ITokenService _tokenService;
        private readonly ILogger _logger;
        private IAuthorizationService _authorizationService;
        private readonly IMapper _mapper;

        public AccountController(UserManager<AppUser> userManager, SignInManager<AppUser> signInManager ,ITokenService tokenService, ILogger<AccountController> logger, IAuthorizationService authorizationService,IMapper mapper)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _tokenService = tokenService;
            _logger = logger;
            _authorizationService = authorizationService;
            _mapper = mapper;
        }

        [HttpPost("register")]
        public async Task<ActionResult<UserDto>> Register(RegisterDto registerDto)
        {
            if (await UserExists(registerDto.Username))
            {
                return BadRequest("Username is taken");
            }

            var user = _mapper.Map<AppUser>(registerDto);

            user.UserName = registerDto.Username.ToLower();

            var result = await _userManager.CreateAsync(user, registerDto.Password);
            if(!result.Succeeded)
            {
                return BadRequest(result.Errors);
            }

            var roleResult = await _userManager.AddToRoleAsync(user, "Employee");
            if (!roleResult.Succeeded) return BadRequest(result.Errors);

            try
            {
                return new UserDto
                {
                    Username = user.UserName,
                    Token = await _tokenService.CreateToken(user),
                    KnownAs = user.KnownAs
                };
            }
            catch(ArgumentNullException ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest("Something went wrong!");
            }
        }

        [HttpPost("login")]
        public async Task<ActionResult<UserDto>> Login(LoginDto loginDto)
        {
            var user = await _userManager.Users.SingleOrDefaultAsync(a => a.UserName == loginDto.Username.ToLower());

            if (user == null)
                return Unauthorized("Invalid username");


            var result = await _signInManager.CheckPasswordSignInAsync(user, loginDto.Password, false);
            
            if (!result.Succeeded)
                return Unauthorized();

            try
            {
                return new UserDto
                {
                    Username = user.UserName
                    ,Token = await _tokenService.CreateToken(user)
                    ,PhotoUrl = user.Photo?.Url
                    ,KnownAs = user.KnownAs
                };
            }
            catch (ArgumentNullException ex)
            {                
                _logger.LogError(ex.Message);
                return Unauthorized("Something went wrong!");
            }
           
        }

        private async Task<bool> UserExists(string username)
        {
            return  await _userManager.Users.AnyAsync(a => a.UserName == username.ToLower());
        }
    }
}
