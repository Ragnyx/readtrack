﻿using Api.Controllers.Base;
using Api.Data.Wrapper;
using Api.Extensions;
using Api.Models.DTO;
using Api.Models.Entities;
using Api.Models.Helpers;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Controllers
{

    public class ProjectController : BaseApiController
    {
        private IRepositoryWrapper _repoWrapper;
        private readonly IMapper _mapper;

        public ProjectController(IRepositoryWrapper repoWrapper, IMapper mapper)
        {
            _repoWrapper = repoWrapper;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ProjectDto>> GetProjects([FromQuery]ProjectParams projectParams)
        {
            projectParams.CurrentUsername = User.GetUsername();

            var projects = _repoWrapper.Projects.GetProjectsDto(projectParams);

            Response.AddPaginationHeader(projects.CurrentPage, projects.PageSize, projects.TotalCount, projects.TotalPages);


            return Ok(projects);
        }

        [HttpGet("{title}")]
        public async Task<ActionResult<ProjectDto>> GetProject(string title)
        {
            return await _repoWrapper.Projects.GetProjectDto(title);
        }

        [HttpPost]
        public async Task<ActionResult<IssueDto>> SaveProject(ProjectDto projectDto)
        {
            var sourceUserId = User.GetUserId();
            var getCurrentUser = await _repoWrapper.Users.GetUser(sourceUserId);


            Project project = _mapper.Map<Project>(projectDto);

            var projectUser = new ProjectUser
            {
                Project = project,
                User = getCurrentUser,
                UserId = sourceUserId
            };

            project.UsersInProject.Add(projectUser);
            getCurrentUser.ProjectsOfUsers.Add(projectUser);

            if (await _repoWrapper.Save()) return NoContent();

            return BadRequest("Failed to create issue");
        }

        [HttpDelete]
        public async Task<ActionResult<ProjectDto>> DeleteProject(int id)
        {
            var project = _repoWrapper.Projects.Find(id);
            if (project is null)
            {
                return NotFound();
            }

            _repoWrapper.Projects.Delete(id);
            if (await _repoWrapper.Save()) return Ok();
            return BadRequest("Failed to delete project");
        }
    }
}
