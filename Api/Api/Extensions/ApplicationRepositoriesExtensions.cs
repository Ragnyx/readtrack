﻿using Api.Data.Abstract;
using Api.Data.Repositories;
using Api.Data.Repositories.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Extensions
{
    public static class ApplicationRepositoriesExtensions
    {
        public static IServiceCollection AddApplicationRepositories(this IServiceCollection services, IConfiguration config)
        {
            //services.AddScoped(typeof(IRepositoryBase<>), typeof(RepositoryBase<>));
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddScoped<IIssueRepository, IssueRepository>();
            services.AddScoped<IProjectRepository, ProjectRepository>();
            services.AddScoped<IProjectUserRepository, ProjectUserRepository>();
            services.AddScoped<IIssueObservatorRepository, IssueObservatorRepository>();
            return services;
        }
    }
}
