﻿using System.Security.Cryptography;
using System.Text;

namespace Api.Services.Authorization
{
    public class AuthorizationService : IAuthorizationService
    {
        public bool ComparePasswords(byte[] userSalt, byte[] userPassword,string passwordFromClient)
        {
            using var hmac = new HMACSHA512(userSalt);

            var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(passwordFromClient));

            for (int i = 0; i < computedHash.Length; i++)
            {
                if (computedHash[i] != userPassword[i])
                    return false;
            }
            return true;
        }
    }
}
