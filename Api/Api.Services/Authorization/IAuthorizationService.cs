﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Services.Authorization
{
    public interface IAuthorizationService
    {
        bool ComparePasswords(byte[] userSalt, byte[] userPassword, string passwordFromClient);
    }
}
