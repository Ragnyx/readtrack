﻿using Api.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Api.Services.Token
{
    public interface ITokenService
    {
        Task<string> CreateToken(AppUser user);
    }
}
