﻿using Api.Data.Properties;
using Api.Models;
using Api.Models.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Api.Data.Seeding
{
    public class Seed
    {
        public static async Task SeedUsers(UserManager<AppUser> userManager
                                           ,RoleManager<AppRole> roleManager)
        {
            if (await userManager.Users.AnyAsync()) return;

            string userData = Resources.UserSeedJson;
            //no users in database
            var users = JsonSerializer.Deserialize<List<AppUser>>(userData); //Deserialize users from json file to object list
            if (users == null) return;
            var roles = new List<AppRole>
            {
                new AppRole {Name = "Employee"},
                new AppRole {Name = "Programmer"},
                new AppRole {Name = "Tester"},
                new AppRole {Name = "Manager"},
                new AppRole {Name = "Admin" }
            };

            foreach (var role in roles)
            {
                await roleManager.CreateAsync(role);
            }

            int index = 0;
            foreach (var user in users)
            {
                using var hmac = new HMACSHA512();

                user.UserName = user.UserName.ToLower();

                await userManager.CreateAsync(user, "password");
                await userManager.AddToRoleAsync(user,index % 2==0 ? "Programmer" : "Tester");
                index++;
            }

            var manager = new AppUser
            {
                UserName = "manager"
            };
            
            await userManager.CreateAsync(manager, "password");
            await userManager.AddToRoleAsync(manager, "Manager");

        }

        public static async Task SeedProjectsWithIssues(DataContext context)
        {
            if (await context.Projects.AnyAsync() || await context.Issues.AnyAsync()) return;

            AppUser appUser = await context.Users.FirstAsync();

            var listOfProjects = new List<Project>();
            //2 projects with 3 issues
            for (int i = 0; i < 13; i++)
            {
                bool visible = (i == 1 || i == 7) ? false : true; //second and eigth project should not be visible 
                Project pr1 = new Project() { Title = "Project " + (i + 1), Description = "Description of project " + +(i + 1),Visible = visible }; 
                listOfProjects.Add(pr1);
            }

            
            context.ProjectUsers.Add(new ProjectUser() { Project = listOfProjects[0], User = appUser });

            //Just 3 first projects will have issues in them
            Issue issue11 = new Issue() { Title = "Issue 1 in Project 1", Description = "Description of issue 1", Created = DateTime.Now, IssueInProject = listOfProjects[0] };
            Issue issue21 = new Issue() { Title = "Issue 2 in Project 1", Description = "Description of issue 2", Created = DateTime.Now, IssueInProject = listOfProjects[0] };
            Issue issue31 = new Issue() { Title = "Issue 3 in Project 1", Description = "Description of issue 3", Created = DateTime.Now, IssueInProject = listOfProjects[0] };

            Issue issue12 = new Issue() { Title = "Issue 1 in Project 2", Description = "Description of issue 1", Created = DateTime.Now, IssueInProject = listOfProjects[1] };
            Issue issue22 = new Issue() { Title = "Issue 2 in Project 2", Description = "Description of issue 2", Created = DateTime.Now, IssueInProject = listOfProjects[1] };
            Issue issue32 = new Issue() { Title = "Issue 2 in Project 2", Description = "Description of issue 2", Created = DateTime.Now, IssueInProject = listOfProjects[1] };



            listOfProjects[0].ProjectIssues.Add(issue11); listOfProjects[0].ProjectIssues.Add(issue21); listOfProjects[0].ProjectIssues.Add(issue31);
            listOfProjects[1].ProjectIssues.Add(issue12); listOfProjects[1].ProjectIssues.Add(issue22); listOfProjects[1].ProjectIssues.Add(issue32);

            context.Projects.AddRange(listOfProjects);

            //context.Projects.Add(pr1); context.Projects.Add(pr2);
            await context.SaveChangesAsync();
        }


    }
}
