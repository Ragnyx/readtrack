﻿using Api.Models;
using Api.Models.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Api.Data
{
    public class DataContext : IdentityDbContext<AppUser,AppRole
                    ,int,IdentityUserClaim<int>,AppUserRole,IdentityUserLogin<int>
                    ,IdentityRoleClaim<int>,IdentityUserToken<int>>
    {
        public DataContext(DbContextOptions options) : base(options)
        {

        }


        public DbSet<Project> Projects { get; set; }
        public DbSet<Issue> Issues { get; set; }
        public DbSet<ProjectUser> ProjectUsers { get; set; }
        public DbSet<IssueObserver> IssueObservers { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //When run from tests
            if (optionsBuilder.IsConfigured)
                return;


            //Note this is only meant for sqlite when using diffrent db that is using connection string rather than an actual file OnOnConfiguring() can be safely deleted 

            //Overwrite saving database location in main project so that in appsettings.json and in startup we will only need to provide database name for sql lite

            //Debugger.Launch();  //For debuging purpose 

            string baseDir = AppDomain.CurrentDomain.BaseDirectory;

            //if "bin" is present, remove all the path starting from "bin" word
            if (baseDir.Contains("bin"))
            {
                int index = baseDir.IndexOf("bin");
                baseDir = baseDir.Substring(0, index);
            }

            //append seperator if necessary
            if (!Path.EndsInDirectorySeparator(baseDir))
            {
                baseDir += Path.DirectorySeparatorChar;
            }


            //TODO: Add a detect changing a db provider and change conf as well

            //Assume that appSettings.json contain a connection with sqlite
            //IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile(@Directory.GetCurrentDirectory() + "/../Api/appsettings.json").Build();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(baseDir).AddJsonFile(baseDir + "/../Api/appsettings.json").Build();

            var connectionString = configuration.GetConnectionString("DefaultConnection");
            string[] connectionStringSplitted = connectionString.Split('=');
            if(connectionStringSplitted.Length >= 2)
            {
                string databaseName = connectionStringSplitted[1];
                string relativePathWithFileName = @"..\Api\" + databaseName;
                string absolutePath = baseDir + relativePathWithFileName;
                optionsBuilder.UseSqlite(@"Data Source=" + absolutePath);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<AppUser>()
                   .HasMany(ur => ur.UserRoles)
                   .WithOne(u => u.User)
                   .HasForeignKey(ur => ur.UserId)
                   .IsRequired();

            modelBuilder.Entity<AppRole>()
                   .HasMany(ur => ur.UserRoles)
                   .WithOne(u => u.Role)
                   .HasForeignKey(ur => ur.RoleId)
                   .IsRequired();

            modelBuilder.Entity<AppUser>().OwnsOne(p => p.Photo);
            modelBuilder.Entity<Issue>()
                        .HasOne(i => i.IssueInProject).WithMany(i => i.ProjectIssues).IsRequired().HasForeignKey(i => i.IssueInProjectId)
                        .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<ProjectUser>()
                        .HasKey(k => new { k.ProjectId, k.UserId });

            modelBuilder.Entity<ProjectUser>()
                        .HasOne(p => p.Project)
                        .WithMany(p => p.UsersInProject)
                        .HasForeignKey(p => p.ProjectId)
                        .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<ProjectUser>()
                        .HasOne(p => p.User)
                        .WithMany(p => p.ProjectsOfUsers)
                        .HasForeignKey(p => p.UserId)
                        .OnDelete(DeleteBehavior.Cascade);


            modelBuilder.Entity<IssueObserver>()
                        .HasKey(k => new { k.IssueId, k.ObserverId });

            modelBuilder.Entity<IssueObserver>()
                        .HasOne(p => p.Issue)
                        .WithMany(p => p.ObservingUsers)
                        .HasForeignKey(p => p.IssueId)
                        .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<IssueObserver>()
                        .HasOne(p => p.Observer)
                        .WithMany(p => p.ObservingIssues)
                        .HasForeignKey(p => p.ObserverId)
                        .OnDelete(DeleteBehavior.Cascade);


            //modelBuilder.Entity<AppUser>()
            //.HasMany(e => e.Photos)
            //.WithOne()
            //.OnDelete(DeleteBehavior.Cascade); //In case of problems in diffrent databases change it to ClientCascade
        }
    }

    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {
            string baseDir = AppDomain.CurrentDomain.BaseDirectory;

            //if "bin" is present, remove all the path starting from "bin" word
            if (baseDir.Contains("bin"))
            {
                int index = baseDir.IndexOf("bin");
                baseDir = baseDir.Substring(0, index);
            }

            //append seperator if necessary
            if (!Path.EndsInDirectorySeparator(baseDir))
            {
                baseDir += Path.DirectorySeparatorChar;
            }


            //TODO: Add a detect changing a db provider and change conf as well

            //Assume that appSettings.json contain a connection with sqlite
            //IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile(@Directory.GetCurrentDirectory() + "/../Api/appsettings.json").Build();
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(baseDir).AddJsonFile(baseDir + "/../Api/appsettings.json").Build();

            var connectionString = configuration.GetConnectionString("DefaultConnection");
            string[] connectionStringSplitted = connectionString.Split('=');
            if (connectionStringSplitted.Length >= 2)
            {

                string databaseName = connectionStringSplitted[1];
                string relativePathWithFileName = @"..\Api\" + databaseName;
                string absolutePath = baseDir + relativePathWithFileName;

                Debug.WriteLine("Database was saved in/removed from " + absolutePath); //To Debug
                Console.WriteLine("Database was saved in/removed from " + absolutePath); //To Output/Powershell
                var builder = new DbContextOptionsBuilder<DataContext>();
                builder.UseSqlite(@"Data Source=" + absolutePath);
                return new DataContext(builder.Options);
            }
            else
            {
                //If appsettings is not correctly set create database in current project
                Debug.WriteLine("Database was saved in/removed from  " + baseDir + connectionString); //To Debug
                Console.WriteLine("Database was saved in/removed from " + baseDir + connectionString); //To Output/Powershell

                var builder = new DbContextOptionsBuilder<DataContext>();
                builder.UseSqlite(connectionString);
                return new DataContext(builder.Options);
            }

        }
    }
}
