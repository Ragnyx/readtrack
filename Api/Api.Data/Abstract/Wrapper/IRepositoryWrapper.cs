﻿using Api.Data.Repositories.Interfaces;
using System.Threading.Tasks;

namespace Api.Data.Wrapper
{
    public interface IRepositoryWrapper
    {
        IUserRepository Users { get; }
        IProjectRepository Projects { get; }
        IIssueRepository Issues { get; }
        IProjectUserRepository ProjectUserRepository { get; }
        Task<bool> Save();
    }
}
