﻿using Api.Data.Repositories;
using Api.Data.Repositories.Interfaces;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Api.Data.Wrapper
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private DataContext _dataContext;
        private IUserRepository _userRepository;
        private IProjectRepository _projectRepository;
        private IIssueRepository _issueRepository;
        private IProjectUserRepository _projectUserRepository;
        private IMapper _mapper;

        public RepositoryWrapper(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }


        public IUserRepository Users 
        {
            get
            {
                if (_userRepository == null)
                {
                    _userRepository = new UserRepository(_dataContext);
                }
                return _userRepository;
            }
        }


        public IProjectRepository Projects
        {
            get
            {
                if (_projectRepository == null)
                {
                    _projectRepository = new ProjectRepository(_dataContext, _mapper);
                }
                return _projectRepository;
            }
        }

        public IIssueRepository Issues
        {
            get
            {
                if (_issueRepository == null)
                {
                    _issueRepository = new IssueRepository(_dataContext,_mapper);
                }
                return _issueRepository;
            }
        }

        public IProjectUserRepository ProjectUserRepository
        {
            get
            {
                if (_projectUserRepository == null)
                {
                    _projectUserRepository = new ProjectUserRepository(_dataContext);
                }
                return _projectUserRepository;
            }
        }

        public async Task<bool> Save()
        {
           return await _dataContext.SaveChangesAsync() > 0;
        }
    }
}
