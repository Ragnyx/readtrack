﻿using Api.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Api.Data.Abstract
{
    public interface IRepositoryBase<T> where T : class
    {
        IQueryable<T> FindAll();
        T Find(int id);
        IEnumerable<T> FindManyByCondition(Expression<Func<T, bool>> filter, 
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = "");
        T FindSingleByCondition(Expression<Func<T, bool>> filter);
        void Create(T entity);
        void Update(T entity);
        void Delete(int id);
        void Delete(T entity);
    }
}
