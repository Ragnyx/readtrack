﻿using Api.Data.Abstract;
using Api.Data.Repositories.Interfaces;
using Api.Models;
using Api.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Api.Data.Repositories
{
    public class ProjectUserRepository :  RepositoryBase<ProjectUser>, IProjectUserRepository
    {
        private readonly DataContext _dataContext;

        public ProjectUserRepository(DataContext dataContext) : base(dataContext)
        {
            _dataContext = dataContext;
        }

        public ProjectUser GetProjectUser(int projectId, int userId)
        {
            return _dataContext.ProjectUsers.Find(projectId, userId);
        }

        public IEnumerable<AppUser> GetProjectUsers(int projectId)
        {
            var users = _dataContext.Users.OrderBy(u => u.UserName).AsQueryable();
            var projectUsers = _dataContext.ProjectUsers.AsQueryable();

            projectUsers = projectUsers.Where(a => a.ProjectId == projectId);
            users = projectUsers.Select(user => user.User);

            return users;
        }

        public Project GetProjectWithUsers(int projectId)
        {
            return _dataContext.Projects
                   .Include(x => x.UsersInProject)
                   .FirstOrDefault(x => x.Id == projectId);
        }

        public IQueryable<Project> GetUserProjects(int userId)
        {
            var projects = _dataContext.Projects.OrderBy(u => u.Title).AsQueryable();
            var projectUsers = _dataContext.ProjectUsers.AsQueryable();

            projectUsers = projectUsers.Where(a => a.UserId == userId);
            
            projects = projectUsers.Select(project => project.Project);

            
            return projects;
        }

        public AppUser GetUserWithProjects(int userId)
        {
            return _dataContext.Users
                    .Include(x => x.ProjectsOfUsers)
                    .FirstOrDefault(x => x.Id == userId);
        }
    }
}
