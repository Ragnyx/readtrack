﻿using Api.Data.Abstract;
using Api.Data.Repositories.Interfaces;
using Api.Models;
using Api.Models.DTO;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Data.Repositories
{
    public class UserRepository : RepositoryBase<AppUser>, IUserRepository
    {
        private readonly DataContext _dataContext;

        public UserRepository(DataContext dataContext) : base(dataContext)
        {
            _dataContext = dataContext;
        }

        public bool AnyUser(System.Linq.Expressions.Expression<Func<AppUser, bool>> filter)
        {
            return _dataContext.Users.Any(filter);
        }

        public AppUser GetUser(string username)
        {
            return _dataContext.Users.Where(x => x.UserName == username).SingleOrDefault();
        }

        public async Task<AppUser> GetUser(int id)
        {
            return await _dataContext.Users.Where(x => x.Id == id).SingleOrDefaultAsync();
        }

        public IEnumerable<AppUser> GetUsers()
        {
            return _dataContext.Users.ToList();
        }

        public async Task<AppUser> GetUserByUsernameAsync(string username)
        {
            return await _dataContext.Users
            .Include(p => p.Photo)
            .SingleOrDefaultAsync(x => x.UserName == username);
        }



    }
}
