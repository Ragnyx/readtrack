﻿using Api.Data.Abstract;
using Api.Data.Repositories.Interfaces;
using Api.Models;
using Api.Models.DTO;
using Api.Models.Entities;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Data.Repositories
{
    public class IssueRepository : RepositoryBase<Issue>, IIssueRepository
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public IssueRepository(DataContext dataContext,IMapper mapper) : base(dataContext)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task<IssueDto> GetIssueDto(int id)
        {
            return await _dataContext.Issues
                                .Where(x => x.Id == id)
                                .ProjectTo<IssueDto>(_mapper.ConfigurationProvider)
                                .SingleOrDefaultAsync();
        }
    }
}
