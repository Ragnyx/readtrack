﻿using Api.Data.Abstract;
using Api.Data.Repositories.Interfaces;
using Api.Models;
using Api.Models.DTO;
using Api.Models.Entities;
using Api.Models.Helpers;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Data.Repositories
{
    public class  IssueObservatorRepository : RepositoryBase<IssueObserver>, IIssueObservatorRepository
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public IssueObservatorRepository(DataContext dataContext, IMapper mapper) : base(dataContext)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public IEnumerable<Issue> GetIssuesOfObserver(int observerId)
        {
            var issues = _dataContext.Issues.AsQueryable();
            var issueObserver = _dataContext.IssueObservers.AsQueryable();

            issueObserver = issueObserver.Where(a => a.ObserverId == observerId);

            issues = issueObserver.Select(issue => issue.Issue);


            return issues;
        }

        public IEnumerable<AppUser> GetObserversOfIssue(int issueId)
        {
            var users = _dataContext.Users.AsQueryable();
            var issueObserver = _dataContext.IssueObservers.AsQueryable();

            issueObserver = issueObserver.Where(a => a.IssueId == issueId);

            users = issueObserver.Select(user => user.Observer);


            return users;
        }
    }
}
