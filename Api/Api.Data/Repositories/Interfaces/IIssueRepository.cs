﻿using Api.Data.Abstract;
using Api.Models.DTO;
using Api.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Api.Data.Repositories.Interfaces
{
    public interface IIssueRepository :  IRepositoryBase<Issue>
    {
        Task<IssueDto> GetIssueDto(int id);
    }
}
