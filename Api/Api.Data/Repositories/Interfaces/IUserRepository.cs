﻿using Api.Data.Abstract;
using Api.Models;
using Api.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Api.Data.Repositories.Interfaces
{
    public interface IUserRepository : IRepositoryBase<AppUser>
    {
        IEnumerable<AppUser> GetUsers();
        AppUser GetUser(string username);
        Task<AppUser> GetUser(int id);
        bool AnyUser(Expression<Func<AppUser, bool>> filter);
        Task<AppUser> GetUserByUsernameAsync(string username);
    }
}
