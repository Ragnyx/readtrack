﻿using Api.Data.Abstract;
using Api.Models.DTO;
using Api.Models.Entities;
using Api.Models.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Api.Data.Repositories.Interfaces
{
    public interface IProjectRepository :  IRepositoryBase<Project>
    {
        IEnumerable<Project> GetProjects();
        Project GetProject(string title);

        PagedList<ProjectDto> GetProjectsDto(ProjectParams projectParams);
        Task<ProjectDto> GetProjectDto(string title);
    }
}
