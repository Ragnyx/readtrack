﻿
using Api.Data.Abstract;
using Api.Models;
using Api.Models.DTO;
using Api.Models.Entities;
using Api.Models.Helpers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Api.Data.Repositories.Interfaces
{
    public interface IIssueObservatorRepository : IRepositoryBase<IssueObserver>
    {
        IEnumerable<Issue> GetIssuesOfObserver(int observerId);
        IEnumerable<AppUser> GetObserversOfIssue(int issueId);
    }
}
