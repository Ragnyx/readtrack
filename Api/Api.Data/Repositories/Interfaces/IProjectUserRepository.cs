﻿using Api.Data.Abstract;
using Api.Models;
using Api.Models.DTO;
using Api.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Api.Data.Repositories.Interfaces
{
    public interface IProjectUserRepository : IRepositoryBase<ProjectUser>
    {
        ProjectUser GetProjectUser(int projectId, int userId);
        IQueryable<Project> GetUserProjects(int userId); //If we are only interesed in projects of given user
        IEnumerable<AppUser> GetProjectUsers(int projectId); //If we are only interesed in users of given project

        AppUser GetUserWithProjects(int userId);
        Project GetProjectWithUsers(int projectId);

    }
}
