﻿using Api.Data.Abstract;
using Api.Data.Repositories.Interfaces;
using Api.Models.DTO;
using Api.Models.Entities;
using Api.Models.Helpers;
using Api.Models.Helpers.Pagination;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Data.Repositories
{
    public class ProjectRepository :  RepositoryBase<Project>, IProjectRepository
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;


        public ProjectRepository(DataContext dataContext,IMapper mapper) : base(dataContext)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public IEnumerable<Project> GetProjects()
        {
            return _dataContext.Projects.Include(i => i.ProjectIssues).ToList();
        }

        public Project GetProject(string title)
        {
            return _dataContext.Projects.Include(i => i.ProjectIssues).
                        Where(x => x.Title == title).SingleOrDefault();
        }

        //TODO: Think about adding another method wchich will no take any params to just return list of projects without any filters
        public PagedList<ProjectDto> GetProjectsDto(ProjectParams projectParams)
        {

            List<Project> projects = _dataContext.Projects.Where(a => a.Visible == true).ToList();
            //TODO: Write unit test testing wheter by default only not visible projects will be retunred!
            IQueryable<Project> query = _dataContext.Projects.AsQueryable();


            //ProjectTo must be after this call since acutal selecting is doing in that method


            //If Flag is set then show all projects otherwise show only visible projects
            if (!projectParams.HiddenProjectsShow)
            {
                query =  query.Where(a => a.Visible == true);
            }

            if(projectParams.Title != null && projectParams.Title != string.Empty)
            {
                query = query.Where(p => projectParams.Title == p.Title);
            }

            query = projectParams.OrderBy switch
            {
                "title" => query.OrderBy(p => p.Title),
                _ => query.OrderBy(p => p.Id)
            };

            return  PagedList<ProjectDto>.CreateAsync(query.ProjectTo<ProjectDto>(_mapper
                .ConfigurationProvider)
                ,projectParams.PageNumber, projectParams.PageSize);
        }

        public async Task<ProjectDto> GetProjectDto(string title)
        {
            return await _dataContext.Projects
                               .Where(x => x.Title == title)
                               .ProjectTo<ProjectDto>(_mapper.ConfigurationProvider)
                               .SingleOrDefaultAsync();
        }
    }
}
