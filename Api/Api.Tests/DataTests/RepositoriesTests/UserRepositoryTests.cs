﻿using Api.Data;
using Api.Data.Repositories;
using Api.Models;
using Api.Models.AutoMapper;
using Api.Models.DTO;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Api.Tests.DataTests.RepositoriesTests
{
    public class UserRepositoryTests : IClassFixture<UserRepositoryFixture>
    {
        UserRepositoryFixture _fixture;

        public UserRepositoryTests(UserRepositoryFixture fixture)
        {
            _fixture = fixture;
        }


        [Fact]
        public void GetMemebers()
        {
            UserRepository userRepository = new UserRepository(_fixture.DataContext);
            IEnumerable<AppUser> usersList = userRepository.GetUsers();
            Assert.Equal(usersList.Count(), _fixture.DataContext.Users.Count());
        }

        [Fact]
        public void GetMemeber()
        {
            UserRepository userRepository = new UserRepository(_fixture.DataContext);
            AppUser appUser = userRepository.GetUser(_fixture.NameOfFirstUser);
            Assert.Equal(_fixture.NameOfFirstUser, appUser.UserName);
        }
    }
}
