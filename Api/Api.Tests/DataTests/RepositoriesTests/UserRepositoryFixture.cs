﻿using Api.Data;
using Api.Models;
using Api.Models.AutoMapper;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Tests.DataTests.RepositoriesTests
{
    public class UserRepositoryFixture : IDisposable
    {
        public string NameOfFirstUser { get => "Larry York"; }
        public IEnumerable<AppUser> UsersInMemory { get; }
        public DbContextOptions Options  { get; }
        public DataContext DataContext { get; private set; }

        public UserRepositoryFixture()
        {
            Options = new DbContextOptionsBuilder<DataContext>()
               .UseInMemoryDatabase(databaseName: "ReadTrackDataBase")
               .Options;
            var config = new MapperConfiguration(cfg => cfg.AddProfile<AutoMapperProfiles>());
            var user1 = new AppUser()
            {
                Id = 1,
                UserName = NameOfFirstUser,
                DateOfBirth = DateTime.Now.AddYears(-25),
                KnownAs = "Larry",
                Location = "Room 47"
            };

            var user2 = new AppUser()
            {
                Id = 2,
                UserName = "Tom Manny",
                DateOfBirth = DateTime.Now.AddYears(-30),
                KnownAs = "Tommy",
                Location = "Room 33"
            };

            UsersInMemory = new List<AppUser>() { user1, user2 };

            DataContext = new DataContext(Options);
            DataContext.Users.AddRange(UsersInMemory);
            DataContext.SaveChanges();
        }

        public void Dispose()
        {
            DataContext.Dispose();
        }
    }
}
