﻿using Api.Models;
using Api.Models.AutoMapper;
using Api.Models.DTO;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Api.Tests.Models.AutoMapper
{
    /// <summary>
    /// Test class for Automapper unit tests will be written only for custom mapping
    /// </summary>
    public class AutoMapperProfilesTests
    {
        //[Fact]
        //public void AutoMapper_Configuration_IsValid()
        //{
        //    //Every single member has correlation with destination type. It may not be the right one (since there are always exception cases),
        //    //but it at least tests that every property is moved from source type to destination.
        //    var config = new MapperConfiguration(cfg => cfg.AddProfile<AutoMapperProfiles>());
        //    config.AssertConfigurationIsValid();
        //}

        [Fact]
        public void AutoMapper_AppUser_Date_of_birth_to_MemberDto_Age_IsValid()
        {
            int age = 3;
            var config = new MapperConfiguration(cfg => cfg.AddProfile<AutoMapperProfiles>());
            IMapper mapper = config.CreateMapper();

            var user = new AppUser()
            { DateOfBirth = DateTime.Now.AddYears(-age) };

            MemberDto memberDto = mapper.Map<MemberDto>(user);

            Assert.Equal(age, memberDto.Age);

        }
    }
}
