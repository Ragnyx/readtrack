﻿using Api.Controllers;
using Api.Data.Repositories.Interfaces;
using Api.Data.Wrapper;
using Api.Models;
using Api.Models.AutoMapper;
using Api.Models.DTO;
using Api.Services.Files;
using Api.Tests.SharedFixture;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Api.Tests.ApiTests
{
    public class UsersControllerTests : IClassFixture<AutoMapperFixture>
    {
        private IMapper _mapper;
        private List<AppUser> _list = new List<AppUser>();
        private string _nameOfFirstUser = "User1";

        public UsersControllerTests(AutoMapperFixture autoMapperFixture)
        {
            _mapper = autoMapperFixture.Mapper;
            var user1 = new AppUser()
            {
                Id = 1,
                UserName = _nameOfFirstUser,
                DateOfBirth = DateTime.Now.AddYears(-25),
                KnownAs = "Larry",
                Location = "Room 47"
            };

            var user2 = new AppUser()
            {
                Id = 2,
                UserName = "Tom Manny",
                DateOfBirth = DateTime.Now.AddYears(-30),
                KnownAs = "Tommy",
                Location = "Room 33"
            };

            _list = new List<AppUser>() { user1, user2 };
        }

        [Fact]
        public void Get_Member_Should_Return_MemberDto()
        {
            //Arrange
            var userId = 1;
            var expected = "AAA";
            var product = new AppUser() { UserName = expected, Id = userId };

            var usersRepositoryMock = new Mock<IUserRepository>();
            usersRepositoryMock.Setup(m => m.GetUser(It.IsAny<string>())).Returns(product).Verifiable();

            var unitOfWorkMock = new Mock<IRepositoryWrapper>();
            unitOfWorkMock.Setup(m => m.Users).Returns(usersRepositoryMock.Object);

            var photoServiceMock = new Mock<IPhotoService>();

            var usersController = new UsersController(unitOfWorkMock.Object, _mapper, photoServiceMock.Object);
            var result = usersController.GetUser(expected);

            var items = Assert.IsType<MemberDto>(result.Value);
            MemberDto member = result.Value;
            Assert.Equal(expected, member.Username);
        }

        [Fact]
        public void Get_Members_Should_Return_Correct_First_Member_Dto()
        {
            //Arrange

            var usersRepositoryMock = new Mock<IUserRepository>();
            usersRepositoryMock.Setup(m => m.GetUsers()).Returns(_list).Verifiable();

            var unitOfWorkMock = new Mock<IRepositoryWrapper>();
            unitOfWorkMock.Setup(m => m.Users).Returns(usersRepositoryMock.Object);

            var photoServiceMock = new Mock<IPhotoService>();

            var usersController = new UsersController(unitOfWorkMock.Object, _mapper, photoServiceMock.Object);
            var result = usersController.GetUsers();
            ObjectResult objectResult = result.Result as ObjectResult;

            IEnumerable<MemberDto> memberDtoList = (IEnumerable<MemberDto>)objectResult.Value;
            MemberDto memberDto = memberDtoList.First();

            Assert.Equal(memberDto.Username, _nameOfFirstUser);
        }

        [Fact]
        public void Get_Members_Should_Return_Correct_Size()
        {
            //Arrange

            var usersRepositoryMock = new Mock<IUserRepository>();
            usersRepositoryMock.Setup(m => m.GetUsers()).Returns(_list).Verifiable();

            var unitOfWorkMock = new Mock<IRepositoryWrapper>();
            unitOfWorkMock.Setup(m => m.Users).Returns(usersRepositoryMock.Object);

            var photoServiceMock = new Mock<IPhotoService>();


            var usersController = new UsersController(unitOfWorkMock.Object, _mapper, photoServiceMock.Object);
            var result = usersController.GetUsers();
            ObjectResult objectResult = result.Result as ObjectResult;

            IEnumerable<MemberDto> memberDtoList = (IEnumerable<MemberDto>)objectResult.Value;

            Assert.Equal(memberDtoList.Count(), _list.Count);  //Check if list length from "db" is the same as from controller
        }
    }
}
