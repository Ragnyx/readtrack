﻿using Api.Controllers;
using Api.Data.Repositories.Interfaces;
using Api.Data.Wrapper;
using Api.Models;
using Api.Models.AutoMapper;
using Api.Models.DTO;
using Api.Services.Authorization;
using Api.Services.Token;
using Api.Tests.DataTests.RepositoriesTests;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Api.Tests.ApiTests
{
    public class AccountControllerTests
    {
        //    [Fact]
        //    public void After_successfull_login_return_userDto()
        //    {
        //        var userId = 1;
        //        var expected = "AAA";
        //        var passwordSalt = "72AE25495A7981C40622D49F9A52E4F1565C90F048F59027BD9C8C8900D5C3D8";
        //        var product = new AppUser() { UserName = expected, Id = userId };

        //        var usersRepositoryMock = new Mock<IUserRepository>();
        //        usersRepositoryMock.Setup(m => m.FindSingleByCondition(It.IsAny<Expression<Func<AppUser, bool>>>())).Returns(product).Verifiable();

        //        var unitOfWorkMock = new Mock<IRepositoryWrapper>();
        //        unitOfWorkMock.Setup(m => m.Users).Returns(usersRepositoryMock.Object);

        //        var logger = new Mock<ILogger<AccountController>>();

        //        var tokenMock = new Mock<ITokenService>();

        //        tokenMock.Setup(m => m.CreateToken(It.IsAny<AppUser>())).Returns(Task.FromResult("example token"));

        //        var authorizationService = new Mock<IAuthorizationService>();
        //        authorizationService.Setup(m => m.ComparePasswords(It.IsAny<byte[]>(), It.IsAny<byte[]>(), It.IsAny<string>())).Returns(true);

        //        var config = new MapperConfiguration(cfg => cfg.AddProfile<AutoMapperProfiles>());
        //        IMapper mapper = config.CreateMapper();


        //        AccountController accountController = new AccountController(unitOfWorkMock.Object, tokenMock.Object, logger.Object, authorizationService.Object, mapper);
        //        Microsoft.AspNetCore.Mvc.ActionResult<UserDto> actionResult = accountController.Login(new LoginDto() { Username = expected });
        //        UserDto value = actionResult.Value;
        //        Assert.NotNull(value);
        //    }

        //    [Fact]
        //    public void After_wrong_password_return_unauthorized()
        //    {
        //        var userId = 1;
        //        var expected = "AAA";
        //        var passwordSalt = "72AE25495A7981C40622D49F9A52E4F1565C90F048F59027BD9C8C8900D5C3D8";
        //        var product = new AppUser() { UserName = expected, Id = userId};

        //        var usersRepositoryMock = new Mock<IUserRepository>();
        //        usersRepositoryMock.Setup(m => m.FindSingleByCondition(It.IsAny<Expression<Func<AppUser, bool>>>())).Returns(product).Verifiable();

        //        var unitOfWorkMock = new Mock<IRepositoryWrapper>();
        //        unitOfWorkMock.Setup(m => m.Users).Returns(usersRepositoryMock.Object);

        //        var logger = new Mock<ILogger<AccountController>>();

        //        var tokenMock = new Mock<ITokenService>();
        //        tokenMock.Setup(m => m.CreateToken(It.IsAny<AppUser>())).Returns("example token");

        //        var authorizationService = new Mock<IAuthorizationService>();
        //        authorizationService.Setup(m => m.ComparePasswords(It.IsAny<byte[]>(), It.IsAny<byte[]>(), It.IsAny<string>())).Returns(false);

        //        var config = new MapperConfiguration(cfg => cfg.AddProfile<AutoMapperProfiles>());
        //        IMapper mapper = config.CreateMapper();

        //        AccountController accountController = new AccountController(unitOfWorkMock.Object, tokenMock.Object, logger.Object, authorizationService.Object, mapper);
        //        Microsoft.AspNetCore.Mvc.ActionResult<UserDto> actionResult = accountController.Login(new LoginDto() { Username = expected });
        //        var value2 = actionResult.Result as ObjectResult;
        //        UserDto value = actionResult.Value;


        //        int statusCodeReturned = value2.StatusCode.Value;
        //        int unauthorized = (int)HttpStatusCode.Unauthorized;
        //        Assert.Equal(statusCodeReturned, unauthorized);
        //    }


        //    [Fact]
        //    public void After_wrong_login_return_unauthorized()
        //    {
        //        AppUser appUser = null;

        //        var usersRepositoryMock = new Mock<IUserRepository>();
        //        usersRepositoryMock.Setup(m => m.FindSingleByCondition(It.IsAny<Expression<Func<AppUser, bool>>>())).Returns(appUser).Verifiable();

        //        var unitOfWorkMock = new Mock<IRepositoryWrapper>();
        //        unitOfWorkMock.Setup(m => m.Users).Returns(usersRepositoryMock.Object);

        //        var logger = new Mock<ILogger<AccountController>>();

        //        var tokenMock = new Mock<ITokenService>();
        //        tokenMock.Setup(m => m.CreateToken(It.IsAny<AppUser>())).Returns("example token");

        //        var authorizationService = new Mock<IAuthorizationService>();
        //        authorizationService.Setup(m => m.ComparePasswords(It.IsAny<byte[]>(), It.IsAny<byte[]>(), It.IsAny<string>())).Returns(false);

        //        var config = new MapperConfiguration(cfg => cfg.AddProfile<AutoMapperProfiles>());
        //        IMapper mapper = config.CreateMapper();

        //        AccountController accountController = new AccountController(unitOfWorkMock.Object, tokenMock.Object, logger.Object, authorizationService.Object, mapper);
        //        Microsoft.AspNetCore.Mvc.ActionResult<UserDto> actionResult = accountController.Login(new LoginDto());
        //        var value2 = actionResult.Result as ObjectResult;
        //        UserDto value = actionResult.Value;


        //        int statusCodeReturned = value2.StatusCode.Value;
        //        int unauthorized = (int)HttpStatusCode.Unauthorized;
        //        Assert.Equal(statusCodeReturned, unauthorized);
        //    }


        //    [Fact]
        //    public void When_registering_after_checking_same_user_exitsts_return_bad_request()
        //    {
        //        var usersRepositoryMock = new Mock<IUserRepository>();
        //        usersRepositoryMock.Setup(m => m.AnyUser(It.IsAny<Expression<Func<AppUser, bool>>>())).Returns(true).Verifiable();

        //        var unitOfWorkMock = new Mock<IRepositoryWrapper>();
        //        unitOfWorkMock.Setup(m => m.Users).Returns(usersRepositoryMock.Object);

        //        var logger = new Mock<ILogger<AccountController>>();

        //        var tokenMock = new Mock<ITokenService>();
        //        tokenMock.Setup(m => m.CreateToken(It.IsAny<AppUser>())).Returns("example token");

        //        var authorizationService = new Mock<IAuthorizationService>();
        //        authorizationService.Setup(m => m.ComparePasswords(It.IsAny<byte[]>(), It.IsAny<byte[]>(), It.IsAny<string>())).Returns(false);

        //        var config = new MapperConfiguration(cfg => cfg.AddProfile<AutoMapperProfiles>());
        //        IMapper mapper = config.CreateMapper();

        //        AccountController accountController = new AccountController(unitOfWorkMock.Object, tokenMock.Object, logger.Object, authorizationService.Object, mapper);
        //        Microsoft.AspNetCore.Mvc.ActionResult<UserDto> actionResult = accountController.Register(new RegisterDto());
        //        var value2 = actionResult.Result as ObjectResult;
        //        UserDto value = actionResult.Value;


        //        int statusCodeReturned = value2.StatusCode.Value;
        //        int unauthorized = (int)HttpStatusCode.BadRequest;
        //        Assert.Equal(statusCodeReturned, unauthorized);
        //    }

        //    [Fact]
        //    public void After_successfull_registration_return_userDto()
        //    {
        //        var usersRepositoryMock = new Mock<IUserRepository>();
        //        usersRepositoryMock.Setup(m => m.AnyUser(It.IsAny<Expression<Func<AppUser, bool>>>())).Returns(false).Verifiable();

        //        var unitOfWorkMock = new Mock<IRepositoryWrapper>();
        //        unitOfWorkMock.Setup(m => m.Users).Returns(usersRepositoryMock.Object);

        //        var logger = new Mock<ILogger<AccountController>>();

        //        var tokenMock = new Mock<ITokenService>();
        //        tokenMock.Setup(m => m.CreateToken(It.IsAny<AppUser>())).Returns("example token");

        //        var authorizationService = new Mock<IAuthorizationService>();
        //        authorizationService.Setup(m => m.ComparePasswords(It.IsAny<byte[]>(), It.IsAny<byte[]>(), It.IsAny<string>())).Returns(false);

        //        var config = new MapperConfiguration(cfg => cfg.AddProfile<AutoMapperProfiles>());
        //        IMapper mapper = config.CreateMapper();

        //        AccountController accountController = new AccountController(unitOfWorkMock.Object, tokenMock.Object, logger.Object, authorizationService.Object, mapper);
        //        Microsoft.AspNetCore.Mvc.ActionResult<UserDto> actionResult = accountController.Register(new RegisterDto() { Username = "UserExample", Password = "PasswordExample" });
        //        var value2 = actionResult.Result as ObjectResult;
        //        UserDto value = actionResult.Value;


        //        Assert.Equal(value.Username, "UserExample".ToLower());
        //    }
    }
}
