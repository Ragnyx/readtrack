﻿using Api.Models.AutoMapper;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Tests.SharedFixture
{
    public class AutoMapperFixture : IDisposable
    {
        public IMapper Mapper { get; private set; }

        public AutoMapperFixture()
        {
            var config = new MapperConfiguration(cfg => cfg.AddProfile<AutoMapperProfiles>());
            IMapper mapper = config.CreateMapper();
            Mapper = mapper;
        }

        public void Dispose()
        {
            Mapper = null;
        }
    }
}
