﻿using Api.Models;
using Api.Services.Token;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Api.Tests.ServicesTests.Token
{
    public class TokenServiceTests
    {
        private ITokenService tokenService;

        public TokenServiceTests()
        {
            var inMemorySettings = new Dictionary<string, string> {
                {"TokenKey", "super secrect user password"},
            };

            IConfiguration configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(inMemorySettings)
                .Build();

            var mockUserManager = new Mock<UserManager<AppUser>>(Mock.Of<IUserStore<AppUser>>(), null, null, null, null, null, null, null, null); ;
            mockUserManager.Setup(x => x.GetRolesAsync(It.IsAny<AppUser>()))
                .Returns(Task.FromResult(new List<string>() as IList<string>));
            tokenService = new TokenService(configuration, mockUserManager.Object);
        }

        [Fact]
        public void Null_user_object_should_throw_exception()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => tokenService.CreateToken(null));
        }

        [Fact]
        public void Null_username_should_throw_exception()
        {
            AppUser appUser = new AppUser()
            {
                UserName = null
            };

            Assert.ThrowsAsync<ArgumentNullException>(() => tokenService.CreateToken(appUser));
        }

        [Fact]
        public void Empty_username_should_throw_exception()
        {
            AppUser appUser = new AppUser()
            {
                UserName = string.Empty
            };

            Assert.ThrowsAsync<ArgumentNullException>(() => tokenService.CreateToken(appUser));
        }

        [Fact]
        public void User_with_username_should_get_token()
        {
            AppUser appUser = new AppUser()
            {
                UserName = "user"
            };

            System.Threading.Tasks.Task<string> te = tokenService.CreateToken(appUser);
            Assert.NotNull(te.Result);
        }
    }
}
