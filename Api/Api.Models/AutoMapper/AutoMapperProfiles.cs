﻿using Api.Models;
using Api.Models.DTO;
using Api.Models.Entities;
using Api.Models.Extensions;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Models.AutoMapper
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<AppUser, MemberDto>()
                .ForMember(dest => dest.MainPhoto,opt => opt.MapFrom(src => src.Photo != null ? src.Photo.Url : null))      
                .ForMember(dest => dest.Age,opt => opt.MapFrom(src => src.DateOfBirth.CalculateAge()));

            CreateMap<ProjectUser, MemberDto>();
            CreateMap<Photo, PhotoDto>();
            CreateMap<Issue, IssueDto>()
                  //.ForMember(a => a.IssueInProjectId, opt => opt.MapFrom(src => src.IssueInProjectId))
                  .ForMember(a => a.Observers, opt => opt.MapFrom(src => src.ObservingUsers.Select(y => y.Observer)))
                  .ForMember(a => a.ProjectTitle, opt => opt.MapFrom(src => src.IssueInProject.Title)).ReverseMap();
                   
            CreateMap<Project, ProjectDto>()
                .ForMember(a  => a.Issues, opt => opt.MapFrom(x => x.ProjectIssues))
                .ForMember(a => a.Members, opt => opt.MapFrom(x => x.UsersInProject.Select(y => y.User))).ReverseMap();
                
            CreateMap<MemberUpdateDto, AppUser>()
                    .ForMember(x => x.Id, opt => opt.Ignore())
                    .ForMember(x => x.UserName, opt => opt.Ignore())
                    .ForMember(x => x.PasswordHash, opt => opt.Ignore())
                    .ForMember(x => x.DateOfBirth, opt => opt.Ignore())
                    .ForMember(x => x.Photo, opt => opt.Ignore())
                    .ForMember(x => x.ProjectsOfUsers, opt => opt.Ignore());

            CreateMap<RegisterDto, AppUser>()
                    .ForMember(x => x.Id, opt => opt.Ignore())
                    .ForMember(x => x.PasswordHash, opt => opt.Ignore())
                    .ForMember(x => x.Photo, opt => opt.Ignore())
                    .ForMember(x => x.ProjectsOfUsers, opt => opt.Ignore());

            CreateMap<IssueObserver, IssueDto>();

        }
    }
}
