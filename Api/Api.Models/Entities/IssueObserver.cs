﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Models.Entities
{
    public class IssueObserver
    {
        public int IssueId { get; set; }
        public Issue Issue { get; set; }

        public int ObserverId { get; set; }
        public AppUser Observer { get; set; }
    }
}
