﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Models.Entities
{
    public class Project : IEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool Visible { get; set; }

        public ICollection<Issue> ProjectIssues { get; set; } = new List<Issue>();
        public ICollection<ProjectUser> UsersInProject { get; set; } = new List<ProjectUser>();

    }
}
