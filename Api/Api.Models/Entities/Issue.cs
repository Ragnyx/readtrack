﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Models.Entities
{
    public class Issue : IEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public DateTime Created { get; set; } = DateTime.Now;
        public DateTime ResolveDate { get; set; }
        public string ResolveNote { get; set; }

        public string Sprite { get; set; }
        public string Revision { get; set; }
        public string Version { get; set; }


        public int IssueInProjectId { get; set; }

        public Project IssueInProject { get; set; }


        public ICollection<IssueObserver> ObservingUsers { get; set; } = new List<IssueObserver>();

    }
}
