﻿using Api.Models.Entities;
using Api.Models.Extensions;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Models
{
    public class AppUser : IdentityUser<int>, IEntity
    {
        public DateTime DateOfBirth { get; set; } //We need to know their age
        public string KnownAs { get; set; } //User Name
        public string Location { get; set; } //Where in company i can find person

        public Photo Photo { get; set; }
        public ICollection<ProjectUser> ProjectsOfUsers { get; set; } = new List<ProjectUser>();
        public ICollection<IssueObserver> ObservingIssues { get; set; } = new List<IssueObserver>();
        public ICollection<AppUserRole> UserRoles { get; set; }

    }
}
