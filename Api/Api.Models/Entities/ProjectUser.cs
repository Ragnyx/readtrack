﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Models.Entities
{
    public class ProjectUser
    {
        public int ProjectId { get; set; }
        public Project Project{ get; set; }

        public int UserId { get; set; }
        public AppUser User { get; set; }
    }
}
