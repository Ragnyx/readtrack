﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Api.Models.DTO
{
    public class RegisterDto
    {
        [Required]
        public string Username { get; set; }
        [Required] public string KnownAs { get; set; }
        [Required] public DateTime DateOfBirth { get; set; }
        [Required] public string Location { get; set; }

        [Required]
        [StringLength(8,MinimumLength = 4)]
        public string Password { get; set; }
    }
}
