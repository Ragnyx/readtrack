﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Models.DTO
{
    public class PhotoDto
    {
        public int Id { get; set; }
        public string Url { get; set; }
    }
}
