﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Models.DTO
{
    public class ProjectUserDto
    {
        public int ProjectId { get; set; }
        public int UserId { get; set; }
    }
}
