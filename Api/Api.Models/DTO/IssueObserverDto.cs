﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Models.DTO
{
    public class IssueObserverDto
    {
        public int IssueId { get; set; }
        public int ObserverId { get; set; }
    }
}
