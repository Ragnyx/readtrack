﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Models.DTO
{
    public class MemberDto
    {
        public int Id { get; set; }
        public string Username { get; set; }

        public int Age { get; set; } //We need to know their age
        public string KnownAs { get; set; } //Used Nick
        public string Location { get; set; } //Location of user
        public string MainPhoto { get; set; }
        public PhotoDto Photo { get; set; }
        public ICollection<ProjectDto> Projects { get; set; }
        public ICollection<IssueDto> ObservingIssues { get; set; }
    }
}
