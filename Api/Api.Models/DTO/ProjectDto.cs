﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Models.DTO
{
    public class ProjectDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool Visible { get; set; }

        public ICollection<IssueDto> Issues { get; set; }
        public ICollection<MemberDto> Members { get; set; }
    }
}
