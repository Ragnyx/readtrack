﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Models.DTO
{
    public class IssueDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public DateTime Created { get; set; }
        public DateTime ResolveDate { get; set; }
        public string ResolveNote { get; set; }

        public string Sprite { get; set; }
        public string Revision { get; set; }
        public string Version { get; set; }


        public string IssueInProjectId { get; set; }

        public string ProjectTitle { get; set; }

        public IEnumerable<MemberDto> Observers { get; set; }
    }
}
