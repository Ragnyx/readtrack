﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Models.DTO
{
    public class MemberUpdateDto
    {
        public string KnownAs { get; set; }
        public string Location { get; set; }
    }
}
