﻿using Api.Models.Helpers.Pagination;
using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Models.Helpers
{
    public class ProjectParams : Params
    {
        public string CurrentUsername { get; set; }
        public bool HiddenProjectsShow { get; set; }
        public string Title { get; set; }

        public string OrderBy { get; set; } = "title";
    }
}
