﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Models.Helpers.Pagination
{
    public abstract class Params
    {
        protected const int MaxPageSize = 50; //maximum amount of pages
        public int PageNumber { get; set; } = 1;
        protected int _pageSize = 10;

        public int PageSize
        {
            get => _pageSize;
            set => _pageSize = (value > MaxPageSize) ? MaxPageSize : value;
        }
    }
}
