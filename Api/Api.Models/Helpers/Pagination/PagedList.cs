﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Models.Helpers
{
    public class PagedList<T> : List<T>
    {
        public int CurrentPage { get; set; } 
        public int TotalPages { get; set; } // Total number of pages
        public int PageSize { get; set; } 
        public int TotalCount { get; set; } // How many items are in a given query i.e only issues with a given type

        public PagedList( IEnumerable<T> items,int count,int pageNumber , int pageSize)
        {
            CurrentPage = pageNumber;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize); //if  count is 10  and pageSize is 5 then total pages will by 2 or 9 /4.5 = 2
            PageSize = pageSize;
            TotalCount = count;
            AddRange(items); //add to List
        }

        public static PagedList<T> CreateAsync(IQueryable<T> source, int pageNumber,int pageSize)
        {
            var count = source.Count();
            var items = source.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
            return new PagedList<T>(items, count, pageNumber, pageSize);
        }
    }
}
