import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { empty, Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { Issue } from 'src/app/_models/issue';
import { Member } from 'src/app/_models/member';
import { Project } from 'src/app/_models/project';
import { IssuesService } from 'src/app/_services/issues.service';
import { MembersService } from 'src/app/_services/members.service';

@Component({
  selector: 'app-issue-save',
  templateUrl: './issue-save.component.html',
  styleUrls: ['./issue-save.component.css']
})
export class IssueSaveComponent implements OnInit {
  @Input() project: Project; //project to which issue will be added
  private projectId: number;
  observers: Member[];
  @ViewChild('selectedObservers') selectedObservers:ElementRef;
  observersToAddToIssue: Set<Member> = new Set<Member>();
  saveIssueForm: FormGroup;
  validationErrors: string[] = [];

  constructor(
    private route: ActivatedRoute
    ,private router: Router
    ,private toastrService:ToastrService
    ,private issueService:IssuesService
    ,private memberService:MembersService
    ,private fb:FormBuilder
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {

      //Change later to simply provide a list with project list
      //TODO: just give a list from which user can select project if someone will provide query param simply active this project in list! 
      if(params['projectId'] === null || params['projectId'] === undefined )
      {
        this.router.navigateByUrl('')
        this.toastrService.error('No project id provided');
      }

      this.projectId = params['projectId'];
      this.memberService.getMembers().subscribe(response => {
        this.observers =  response;
      });
    });

    this.intializeForm();
    
  }

  intializeForm(){
    this.saveIssueForm = this.fb.group({
      title: ['',Validators.required],
      description: ['',Validators.required],
      sprite: ['',Validators.required],
      revision: ['',Validators.required],
      version: ['',Validators.required],
      observers: [],
      issueInProjectId: []
    })
  }

  saveIssue()
  {
    this.saveIssueForm.get('issueInProjectId').setValue(this.projectId);


    if(this.observersToAddToIssue.size > 0)
    {
      this.saveIssueForm.get('observers').setValue(Array.from(this.observersToAddToIssue.values()))
    }
    else
    {
      this.saveIssueForm.get('observers').setValue([]);
    }
    
    this.issueService.saveIssue(this.saveIssueForm.value).subscribe(() => {
      this.toastrService.success('Issue created successfully');
    },error => {
      this.validationErrors = error;
    })
  }

  onChange(newObserver:string) {

    const  choosenObserver = this.observers.find(observer => observer.username === newObserver);
    if(choosenObserver != undefined)
    {
      this.observersToAddToIssue.add(choosenObserver);
    }
  }

  onSelect(deviceValue) {
    console.log(deviceValue);
  }

  onHiddenChange(data:any): void 
  {
    console.log(data);
  }

  removeObserver(index:number)
  {
    var currentIndex = 0;
    for (let currentObserver of this.observersToAddToIssue) 
    {
      if(currentIndex == index)
      {
        this.observersToAddToIssue.delete(currentObserver);
        break;
      }
      currentIndex++;
    }
  }
}
