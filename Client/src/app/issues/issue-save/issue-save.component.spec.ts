import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IssueSaveComponent } from './issue-save.component';

describe('IssueSaveComponent', () => {
  let component: IssueSaveComponent;
  let fixture: ComponentFixture<IssueSaveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IssueSaveComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IssueSaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
