import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Issue } from 'src/app/_models/issue';
import { IssuesService } from 'src/app/_services/issues.service';

@Component({
  selector: 'app-issue-detail',
  templateUrl: './issue-detail.component.html',
  styleUrls: ['./issue-detail.component.css']
})
export class IssueDetailComponent implements OnInit {
  issue: Issue;

  constructor(private issueService:IssuesService,private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.loadIssue();
  }

  loadIssue(){
    this.issueService.getIssue(Number(this.route.snapshot.paramMap.get('id'))).subscribe(issue => {
      console.log(issue);
      this.issue = issue;
    })
  }

}
