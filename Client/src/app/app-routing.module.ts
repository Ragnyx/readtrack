import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from './errors/not-found/not-found.component';
import { ServerErrorComponent } from './errors/server-error/server-error.component';
import { TestErrorsComponent } from './errors/test-errors/test-errors.component';
import { HomeComponent } from './home/home.component';
import { IssueDetailComponent } from './issues/issue-detail/issue-detail.component';
import { IssueListComponent } from './issues/issue-list/issue-list.component';
import { IssueSaveComponent } from './issues/issue-save/issue-save.component';
import { MemberDetailComponent } from './members/member-detail/member-detail.component';
import { MemberEditComponent } from './members/member-edit/member-edit.component';
import { MemberListComponent } from './members/member-list/member-list.component';
import { ProjectDetailComponent } from './projects/project-detail/project-detail.component';
import { ProjectListComponent } from './projects/project-list/project-list.component';
import { ProjectSaveComponent } from './projects/project-save/project-save.component';
import { SettingsPanelComponent } from './settings/settings-panel/settings-panel.component';
import { AuthGuard } from './_guards/auth.guard';
import { PreventUnsavedChangesGuard } from './_guards/prevent-unsaved-changes.guard';
import { SettingsGuard } from './_guards/settings.guard';

const routes: Routes = [
  {path: '',component: HomeComponent} // when user will go to our page without any additional path
  ,{
    path: '',
    runGuardsAndResolvers: 'always',
    canActivate: [AuthGuard],
    children: [
      {path: 'issues',component: IssueListComponent}
      ,{path: 'issues/:id',component: IssueDetailComponent}
      ,{path: 'projects',component: ProjectListComponent}
      ,{path: 'project/:title',component: ProjectDetailComponent}
      ,{path: 'members',component: MemberListComponent}
      ,{path: 'members/:username',component: MemberDetailComponent}
      ,{path: 'member/edit',component: MemberEditComponent,canDeactivate: [PreventUnsavedChangesGuard]}
      ,{path: 'issue/:id',component: IssueDetailComponent}
      ,{path: 'create/issue',component: IssueSaveComponent}
      ,{path: 'create/project',component: ProjectSaveComponent}
      ,{path: 'settings',component: SettingsPanelComponent,canActivate: [SettingsGuard]}
    ]
  }
  ,{path: 'errors', component: TestErrorsComponent}
  ,{path: 'not-found', component: NotFoundComponent}
  ,{path: 'server-error', component: ServerErrorComponent}
  , {path: '**',component: NotFoundComponent,pathMatch: 'full'} //non existing route redirect to ...
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
