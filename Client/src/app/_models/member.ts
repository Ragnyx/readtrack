import { Photo } from "./Photo";

 export interface Member {
        id: number;
        username: string;
        age: number;
        knownAs: string;
        location: string;
        mainPhoto: string;
        photo: Photo;
    }

    