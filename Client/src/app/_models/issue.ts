import { Member } from "./member";

export class Issue {
    id: number;
    title: string;
    description: string;
    created: Date;
    resolveDate: Date;
    resolveNote?: string;
    sprite?: string;
    revision?: string;
    version?: string;
    issueInProjectId: number;
    projectTitle: string;
    observers: Member[];
}