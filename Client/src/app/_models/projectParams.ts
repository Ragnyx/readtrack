import { Project } from "./project";

export class ProjectParams {
    pageNumber = 1;
    pageSize = 5;
    hiddenProjectsShow:boolean;
    title:string;
    orderBy = 'title'
}