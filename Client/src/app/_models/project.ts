import { Issue } from "./issue";
import { Member } from "./member";

export class Project {
    id: number;
    title: string;
    description: string;
    visible: boolean;
    issues: Issue[];
    appUsers: Member[];
}