import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Issue } from '../_models/issue';


@Injectable({
  providedIn: 'root'
})
export class IssuesService {
  baseUrl = environment.apiUrl;

  constructor(private http:HttpClient) { }

  getIssue(id: number){
    return this.http.get<Issue>(this.baseUrl + 'issues/' + id);
  }

  saveIssue(issue:Issue){
    return this.http.post(this.baseUrl + 'issues',issue);
  }
}
