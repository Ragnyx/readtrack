import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { PaginatedResult } from '../_models/pagination';
import { Project } from '../_models/project';
import { ProjectParams } from '../_models/projectParams';
import { User } from '../_models/user';
import { AccountService } from './account.service';
import { PaginationService } from './pagination.service';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {
  baseUrl = environment.apiUrl;
  projectsCache = new Map();
  user:User;
  projectParams:ProjectParams
  userProjects = false;
  
  constructor(private http:HttpClient,private accountService:AccountService,private paginationService:PaginationService) 
  {
    this.accountService.currentUser$.pipe(take(1)).subscribe(user => {
      this.user = user;
    })
    this.projectParams = new ProjectParams(); 
  }

  getProjectParams()
  {
    return this.projectParams;
  }

  setProjectParams(params:ProjectParams)
  {
    this.projectParams = params;
  }

  resetProjectParams(){
    this.projectParams = new ProjectParams();
    return this.projectParams;
  }

  getProjects(projectParams:ProjectParams,userProjects = false) : Observable<PaginatedResult<Project[]>>
  {
    let urlOfProjects = userProjects ? 'users/user-projects' : 'project';
    this.userProjects = userProjects;


    //For all projects return cache 
    if(!this.userProjects)
    {
      var response = this.projectsCache.get(this.projectCacheKey() +  Object.values(projectParams).join('-'));
      if(response)
      {
        return of(response);
      }
    }

    let params = this.paginationService.getPaginationHeaders(projectParams.pageNumber,projectParams.pageSize);

    if(projectParams.hiddenProjectsShow != undefined)
      params = params.append('hiddenProjectsShow',projectParams.hiddenProjectsShow);
    
    if(projectParams.title != undefined)
      params = params.append('title',projectParams.title);
      

    params = params.append('orderBy',projectParams.orderBy);

    //{observe: 'response',params} == get full response back
    return this.paginationService.getPaginatedResult<Project[]>(this.baseUrl + urlOfProjects,params)
    .pipe(map(response => 
    {
      if(!this.userProjects)
      {
        this.projectsCache.set(this.projectCacheKey() + Object.values(projectParams).join('-'),response);
      }
      
      
      return response;
    }));
  }
  
  getProject(title: string){

    if(!this.userProjects)
    {
      const project = [...this.projectsCache.values()]
      .reduce((arr,elem) => arr.concat(elem.result), [])
      .find((project:Project) => project.title === title);

      if(project){
        return of(project);
      }
    }
    
    return this.http.get<Project>(this.baseUrl + 'project/' + title);
  }

  
  saveProject(project:Project)
  {
    return this.http.post(this.baseUrl + 'project',project);
  }


  private projectCacheKey()
  {
    var key =  this.userProjects ? "_userProjects" : "_allProjects";
    console.log(key);
    return key;
  }
  
  deleteProject(id:string)
  {
    return this.http.delete(this.baseUrl + 'project/' + id);
  }
}
