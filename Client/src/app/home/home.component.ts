import { Component, Input, OnInit } from '@angular/core';
import { first, take } from 'rxjs/operators';
import { Project } from '../_models/project';
import { User } from '../_models/user';
import { AccountService } from '../_services/account.service';
import { MembersService } from '../_services/members.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  projects: Project[];
  user: User;  

  registerMode = false;

  constructor(private accountService: AccountService,private memberService:MembersService) { }

  ngOnInit(): void 
  {
      this.accountService.currentUser$.subscribe(user => 
      {
        this.user = user
        if(user != null)
        {
            
        }
      },
      error => {
          console.log("EE:",error);
      });
    // this.accountService.currentUser$.pipe(first()).subscribe(user => 
    //   {
    //     console.log("Wartosc = " + user); 
    //     this.user = user
    //   },
    //   error => {
    //       console.log("EE:",error);
    //   });
  }

  registerToggle()
  {
    this.registerMode = !this.registerMode;
  }

  cancelRegisterMode(event: boolean)
  {
      this.registerMode = event;
  }

}
