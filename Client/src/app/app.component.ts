import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { User } from './_models/user';
import { AccountService } from './_services/account.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Client';
  users: any;

  constructor(private http: HttpClient,private accountService: AccountService)
  {

  }

  ngOnInit() {
    this.setCurrentUser();
    //this.getUsers();
  }

  // getUsers()
  // {
  //   this.http.get('https://localhost:5001/api/users').subscribe(response => {
  //     this.users = response;
  //   }, error => {
  //     console.log(error);
  //   })
  // }

  setCurrentUser(){
    const userJson = localStorage.getItem('user');
    if(userJson !== null)
    {
      const user: User = JSON.parse(userJson);
      this.accountService.setCurrentUser(user);
    }
    
  }

}
