import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Project } from 'src/app/_models/project';
import { ProjectsService } from 'src/app/_services/projects.service';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.css']
})
export class ProjectDetailComponent implements OnInit {
  project:Project;


  constructor(private projectService:ProjectsService,private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.loadProject();
  }

  loadProject(){
    this.projectService.getProject(this.route.snapshot.paramMap.get('title')).subscribe(project => {
      console.log(project);
      this.project = project;
    })
  }
}
