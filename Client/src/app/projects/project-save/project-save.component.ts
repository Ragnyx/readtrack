import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Project } from 'src/app/_models/project';
import { ProjectsService } from 'src/app/_services/projects.service';

@Component({
  selector: 'app-project-save',
  templateUrl: './project-save.component.html',
  styleUrls: ['./project-save.component.css']
})
export class ProjectSaveComponent implements OnInit {
  project:Project;
  @ViewChild('editForm') editForm: NgForm;
  
  constructor(
    private toastrService:ToastrService
    ,private projectService:ProjectsService)
  { 
    this.project = new Project();
  }

  ngOnInit(): void {
  }


  saveProject()
  {
    console.log(this.project);
    this.projectService.saveProject(this.project).subscribe(() => {
      this.toastrService.success('Project created successfully');
      this.editForm.reset(this.project);
    })
  }
}
